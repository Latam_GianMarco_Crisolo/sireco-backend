const mssql = require('mssql');

class MSSQL {
    conexion = null;
    conectado = false;

    constructor() {
        this.conectado = false;
        this.conexion = this.conectarDB();
        /*
            .then(async(pool) => {
                this.conectado = true;
                console.log('Conexion a base de datos realizada');
                var a = await pool.query('SELECT 1');

                console.log(a);
            })
            .catch(e => console.log('Error al conectar a la base de datos'));*/
    }

    async conectarDB() {
        /*
        const config = {
            user: 'test',
            password: 'admin123',
            server: '192.168.0.6',
            database: 'sirecoDB',
            driver: 'tedious',
            options: {
                encrypt: false
            }
        };*/
        const config = {
            user: 'test',
            password: 'admin123$',
            server: 'server-app-sireco.database.windows.net',
            database: 'sirecoDB',
            driver: 'tedious',
            options: {
                encrypt: true
            }
        };
        // resuelve el error de conexion a la base de datos.
        return await mssql.connect(config);
    }

    static instance() {
        return this._instance || (this._instance = new this());
    }
}

module.exports = MSSQL;