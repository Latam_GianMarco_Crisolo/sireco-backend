const MSSQL = require('../database/config');

const buscarBahiaPorCodigo = async(codigo) => {
    const query = `SELECT * FROM bahia WHERE codigo = '${codigo}'`;

    let bahiaDB = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0])
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return bahiaDB;
}

const buscarBahiasPorBloque = async(idBloque) => {
    const query = `SELECT * FROM bahia WHERE id_bloque_contenedores = ${idBloque}`;

    let bahias = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });
    return bahias;
}

const buscarBahias = async(bloque, bahia, contenedor) => {
    let query = 'SELECT bc.id AS "bloque.id", bc.codigo AS "bloque.codigo", bc.filas AS "bloque.filas", bc.columnas AS "bloque.columnas", b.*, COUNT(co.id) AS nroContenedores ';
    query += 'FROM bloque_contenedores bc INNER JOIN bahia b ON bc.id = b.id_bloque_contenedores INNER JOIN celda c ON b.id = c.id_bahia LEFT JOIN contenedor co ON c.id_contenedor = co.id ';

    if (bloque) {
        query += 'WHERE ';
        query += `bc.id = '${bloque}' `;
    }
    if (bahia) {
        if (bloque) query += 'AND ';
        else query += 'WHERE ';

        query += `b.codigo LIKE '${bahia}%' `;
    }

    if (contenedor) {
        if (bloque ||  bahia) query += 'AND ';
        else query += 'WHERE ';
        query += `co.codigo LIKE '${contenedor}%' `;
    }

    query += 'GROUP BY bc.id, bc.codigo, bc.filas, bc.columnas, b.id, b.numero, b.codigo, b.estado, b.id_bloque_contenedores';

    console.log(query);
    let bahias = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => {
            return result.recordset.map(result => {
                result.bloque = {};
                result.bloque.id = result["bloque.id"];
                result.bloque.codigo = result["bloque.codigo"];
                result.bloque.filas = result["bloque.filas"];
                result.bloque.columnas = result["bloque.columnas"];
                delete result["bloque.id"];
                delete result["bloque.codigo"];
                delete result["bloque.filas"];
                delete result["bloque.columnas"];
                return result;
            });
        })
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });
    return bahias;
}

const buscarBahiaPorId = async(idBahia) => {
    const query = `SELECT * FROM bahia where id = ${idBahia}`;

    let bahiaDB = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0])
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return bahiaDB;

}

const obtenerNumeroTotalBahiasPorBloque = async(idBloque) => {

    const query = `SELECT COUNT(id) AS total FROM bahia WHERE id_bloque_contenedores = '${idBloque}'`;

    let total = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0].total)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return total;
}

const crearBahia = async(bahiaDB) => {
    const query = `INSERT INTO bahia (codigo, numero, id_bloque_contenedores, estado) OUTPUT INSERTED.id VALUES ('${bahiaDB.codigo}', ${bahiaDB.numero}, ${bahiaDB.id_bloque_contenedores}, '${bahiaDB.estado}')`;

    const id = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0].id)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return id;
}

const eliminarBahiasPorBloque = async(idBloque) => {
    const query = `DELETE FROM bahia OUTPUT DELETED.id WHERE id_bloque_contenedores = ${idBloque}`;

    const idBahiasBorradas = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return idBahiasBorradas;
}

module.exports = {
    buscarBahiaPorCodigo,
    buscarBahiasPorBloque,
    buscarBahias,
    buscarBahiaPorId,
    obtenerNumeroTotalBahiasPorBloque,
    crearBahia,
    eliminarBahiasPorBloque
}