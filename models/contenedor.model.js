const MSSQL = require('../database/config');

const buscarContenedorPorId = async(id) => {

    const query = `SELECT * FROM contenedor WHERE id = '${id}'`;

    let contenedorDB = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0])
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return contenedorDB;
}

const buscarContenedorPorCodigo = async(codigo) => {
    const query = `SELECT * FROM contenedor WHERE codigo = '${codigo}'`;

    let contenedorDB = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0])
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return contenedorDB;
}

const listarContenedores = async(desde) => {
    let query = 'SELECT * FROM contenedor ORDER BY id';

    if (desde !== undefined) {
        query = `${query} OFFSET ${desde} ROWS FETCH NEXT 5 ROWS ONLY`;
    }

    let contenedorDBArray = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return contenedorDBArray;
}

const buscarContenedores = async(codigo, estado, embarcacion) => {
    let query = `SELECT * FROM contenedor `;

    if (codigo) {
        query += 'WHERE ';
        query += `codigo LIKE '${codigo}%' `;
    }
    if (embarcacion) {
        if (codigo) query += 'AND ';
        else query += 'WHERE ';
        query += `embarcacion LIKE '${embarcacion}%' `;
    }
    if (estado) {
        if (codigo || embarcacion) query += 'AND ';
        else query += 'WHERE ';
        query += `estado = '${estado}'`;
    }

    console.log('QUERY', query);

    let contenedoresDB = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return contenedoresDB;
}

const obtenerNumeroTotalContenedores = async() => {
    const query = 'SELECT COUNT(id) AS total FROM contenedor';

    let numeroTotalContenedores = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0].total)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return numeroTotalContenedores;

}

const crearContenedor = async(contenedorDB) => {
    const query = `INSERT INTO contenedor (codigo,tamano,peso,tipo,fecha_salida,embarcacion,estado) OUTPUT INSERTED.id VALUES ('${contenedorDB.codigo}',${contenedorDB.tamano}, ${contenedorDB.peso}, '${contenedorDB.tipo}', CONVERT(datetime,'${contenedorDB.fecha_salida}',20), '${contenedorDB.embarcacion}', '${contenedorDB.estado}')`

    const id = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0].id)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return id;
}

const actualizarContenedor = async(contenedorDB) => {
    const query = 'UPDATE contenedor SET' +
        ` codigo = '${contenedorDB.codigo}',` +
        ` tamano = ${contenedorDB.tamano},` +
        ` peso = ${contenedorDB.peso},` +
        ` tipo = '${contenedorDB.tipo}',` +
        ` fecha_salida = CONVERT(datetime,'${contenedorDB.fecha_salida}',20),` +
        ` embarcacion = '${contenedorDB.embarcacion}',` +
        ` estado = '${contenedorDB.estado}'` +
        ` OUTPUT INSERTED.id WHERE id = ${contenedorDB.id}`;

    const id = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0].id)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return id;
}

const actualizarEstado = async(idContenedor, estado) => {
    const query = `UPDATE contenedor SET estado = '${estado}' OUTPUT INSERTED.id WHERE id = ${idContenedor}`;

    console.log("que chucha fue ", idContenedor, estado);
    const id = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0].id)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return id;
}

const eliminarContenedor = async(id) => {
    const query = `DELETE FROM contenedor OUTPUT DELETED.id WHERE id = ${id}`;

    const idContenedorBorrado = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0])
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return idContenedorBorrado;
}

module.exports = {
    buscarContenedorPorId,
    buscarContenedorPorCodigo,
    buscarContenedores,
    listarContenedores,
    obtenerNumeroTotalContenedores,
    crearContenedor,
    actualizarContenedor,
    actualizarEstado,
    eliminarContenedor
}