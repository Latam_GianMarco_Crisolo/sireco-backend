const MSSQL = require('../database/config');


const buscarUsuarioPorId = async(id) => {

    const query = `SELECT * FROM usuario WHERE id = '${id}'`;

    let usuarioDB = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0])
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return usuarioDB;
}

const buscarUsuarioPorNombreUsuario = async(usuario) => {
    const query = `SELECT * FROM usuario WHERE usuario = '${usuario}'`;

    let usuarioDB = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0])
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return usuarioDB;
}

const listarUsuarios = async(desde) => {
    let query = 'SELECT * FROM usuario ORDER BY id';

    if (desde !== undefined) {
        query = `${query} OFFSET ${desde} ROWS FETCH NEXT 5 ROWS ONLY`;
    }

    let usuarioDBArray = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return usuarioDBArray;
}

const obtenerNumeroTotalUsuarios = async() => {
    const query = 'SELECT COUNT(id) AS total FROM usuario';

    let numeroTotalUsuarios = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0].total)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return numeroTotalUsuarios;
}

const crearUsuario = async(usuarioDB) => {
    const query = `INSERT INTO usuario (usuario,contrasena,nombre,apellido_pat,apellido_mat,rol,estado) OUTPUT INSERTED.id VALUES ('${usuarioDB.usuario}', '${usuarioDB.contrasena}', '${usuarioDB.nombre}', '${usuarioDB.apellido_pat}', '${usuarioDB.apellido_mat}', '${usuarioDB.rol}', '${usuarioDB.estado}')`;

    const id = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0].id)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return id;

}

const actualizarUsuario = async(usuarioDB) => {
    const query = 'UPDATE usuario SET' +
        ` nombre = '${usuarioDB.nombre}',` +
        ` apellido_pat = '${usuarioDB.apellidoPaterno}',` +
        ` apellido_mat = '${usuarioDB.apellidoMaterno}',` +
        ` rol = '${usuarioDB.rol}'` +
        ` OUTPUT INSERTED.id WHERE id = ${usuarioDB.id}`;

    const id = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0].id)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return id;
}

const eliminarUsuario = async(id) => {
    const query = `DELETE FROM usuario OUTPUT DELETED.id WHERE id = ${id}`;

    const idUsuarioBorrado = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0])
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return idUsuarioBorrado;
}

module.exports = {
    buscarUsuarioPorId,
    buscarUsuarioPorNombreUsuario,
    listarUsuarios,
    obtenerNumeroTotalUsuarios,
    crearUsuario,
    actualizarUsuario,
    eliminarUsuario
}