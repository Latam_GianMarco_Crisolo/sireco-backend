const MSSQL = require('../database/config');

const crearMovimiento = async(movimientoDB) => {
    const query = `INSERT INTO movimiento (numero, tipo, fecha) OUTPUT INSERTED.id VALUES ((SELECT (COALESCE(MAX(m.numero),0) + 1) FROM movimiento m), '${movimientoDB.tipo}', GETDATE())`;

    const id = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0].id)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return id;

}

module.exports = {
    crearMovimiento
}