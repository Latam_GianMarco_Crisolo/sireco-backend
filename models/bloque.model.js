const MSSQL = require('../database/config');

const buscarBloquePorId = async(id) => {
    const query = `SELECT * FROM bloque_contenedores WHERE id = '${id}'`;

    let bloqueDB = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0])
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return bloqueDB;
}

const buscarBLoquePorIdConTotalBahias = async(id) => {
    const query = `SELECT bc.*, COUNT(b.id) AS numBahias FROM bloque_contenedores bc LEFT JOIN bahia b on bc.id = b.id_bloque_contenedores WHERE bc.id = '${id}' GROUP BY bc.id, bc.codigo, bc.columnas, bc.estado, bc.filas`;

    let bloqueDB = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0])
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return bloqueDB;
}

const buscarBloquePorCodigo = async(codigo) => {
    const query = `SELECT * FROM bloque_contenedores WHERE codigo = '${codigo}'`;

    let bloqueDB = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0])
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return bloqueDB;
}

const listarBloquesConTotalBahias = async(desde) => {
    let query = `SELECT bc.*, COUNT(b.id) AS numBahias FROM bloque_contenedores bc LEFT JOIN bahia b on bc.id = b.id_bloque_contenedores GROUP BY bc.id, bc.codigo, bc.columnas, bc.estado, bc.filas ORDER BY bc.id`;

    if (desde !== undefined) {
        query = `${query} OFFSET ${desde} ROWS FETCH NEXT 5 ROWS ONLY`;
    }

    let bloqueDBArray = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return bloqueDBArray;
}

const obtenerNumeroTotalBloques = async() => {
    const query = 'SELECT COUNT(id) AS total FROM bloque_contenedores';

    let numeroTotalBloques = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0].total)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return numeroTotalBloques;
}

const crearBloque = async(bloqueDB) => {
    const query = `INSERT INTO bloque_contenedores (codigo,filas,columnas,estado) OUTPUT INSERTED.id VALUES ('${bloqueDB.codigo}',${bloqueDB.filas}, ${bloqueDB.columnas}, '${bloqueDB.estado}')`;

    const id = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0].id)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return id;
}

const eliminarBloque = async(id) => {
    const query = `DELETE FROM bloque_contenedores OUTPUT DELETED.id WHERE id = ${id}`;

    const idBloqueBorrado = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0])
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return idBloqueBorrado;
}

module.exports = {
    buscarBloquePorId,
    buscarBLoquePorIdConTotalBahias,
    buscarBloquePorCodigo,
    listarBloquesConTotalBahias,
    obtenerNumeroTotalBloques,
    crearBloque,
    eliminarBloque
}