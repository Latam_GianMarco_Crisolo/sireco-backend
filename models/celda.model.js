const MSSQL = require('../database/config');

const buscarCeldaPorFilaColumnaBahia = async(celdaDB) => {
    const query = `SELECT * FROM celda WHERE fila = ${celdaDB.fila} AND columna = ${celdaDB.columna} AND id_bahia = ${celdaDB.id_bahia}`;

    let celda = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0])
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return celda;
}

const buscarCeldasPorBahia = async(idBahia) => {

    let query = 'SELECT ce.*, c.id AS "contenedor.id", c.codigo AS "contenedor.codigo", c.tipo AS "contenedor.tipo", c.tamano AS "contenedor.tamano", c.peso AS "contenedor.peso", c.fecha_salida AS "contenedor.fechaSalida", c.embarcacion AS "contenedor.embarcacion" ';
    query += `FROM celda ce LEFT JOIN contenedor c ON ce.id_contenedor = c.id WHERE ce.id_bahia = ${idBahia} ORDER BY ce.columna , ce.fila DESC`;

    let celdasDB = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => {
            return result.recordset.map(result => {
                if (result["contenedor.id"] != null) {
                    result.contenedor = {};
                    result.contenedor.id = result["contenedor.id"];
                    result.contenedor.codigo = result["contenedor.codigo"];
                    result.contenedor.tipo = result["contenedor.tipo"];
                    result.contenedor.tamano = result["contenedor.tamano"];
                    result.contenedor.peso = result["contenedor.peso"];
                    result.contenedor.fecha_salida = result["contenedor.fechaSalida"];
                    result.contenedor.embarcacion = result["contenedor.embarcacion"];
                }
                delete result["contenedor.id"];
                delete result["contenedor.codigo"];
                delete result["contenedor.tipo"];
                delete result["contenedor.tamano"];
                delete result["contenedor.peso"];
                delete result["contenedor.fechaSalida"];
                delete result["contenedor.embarcacion"];;
                return result;
            });
        })
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });
    return celdasDB;
}

const crearCelda = async(celdaDB) => {
    const query = `INSERT INTO celda (fila, columna, id_bahia ) OUTPUT INSERTED.id VALUES (${celdaDB.fila}, ${celdaDB.columna}, ${celdaDB.id_bahia})`;

    const id = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0].id)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return id;
}

const actualizarContenedor = async(celdaDB) => {
    const query = `UPDATE celda SET id_contenedor = ${celdaDB.id_contenedor} OUTPUT INSERTED.id WHERE id = ${celdaDB.id}`;

    const id = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0].id)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return id;
}

const actualizarContenedorPorFilaColumnaBahia = async(celdaDB) => {
    const query = `UPDATE celda SET id_contenedor = ${celdaDB.id_contenedor} OUTPUT INSERTED.id WHERE fila = ${celdaDB.fila} AND columna = ${celdaDB.columna} AND id_bahia = ${celdaDB.id_bahia}`;

    const id = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0].id)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return id;
}

const eliminarCeldasPorBahia = async(idBahia) => {
    const query = `DELETE FROM celda OUTPUT DELETED.id WHERE id_bahia = ${idBahia}`;

    const idCeldasBorradas = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return idCeldasBorradas;
}

module.exports = {
    buscarCeldaPorFilaColumnaBahia,
    buscarCeldasPorBahia,
    crearCelda,
    actualizarContenedor,
    actualizarContenedorPorFilaColumnaBahia,
    eliminarCeldasPorBahia
}