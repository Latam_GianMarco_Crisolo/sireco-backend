const MSSQL = require('../database/config');

const crearMovimientoDetalle = async(movimientoDetalleDB) => {
    const query = `INSERT INTO movimiento_detalle (id_contenedor, id_celda_inicio, id_celda_fin, tipo, id_movimiento) OUTPUT INSERTED.id VALUES (${movimientoDetalleDB.id_contenedor}, ${movimientoDetalleDB.id_celda_inicio}, ${movimientoDetalleDB.id_celda_fin}, '${movimientoDetalleDB.tipo}', ${movimientoDetalleDB.id_movimiento})`;

    const id = await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => result.recordset[0].id)
        .catch(error => {
            console.log(error);
            throw { ok: false, status: 500, msg: 'Error inesperado... revisar log' };
        });

    return id;
}


module.exports = {
    crearMovimientoDetalle
}