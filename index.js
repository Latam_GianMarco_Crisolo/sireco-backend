require('dotenv').config();

const express = require('express');
const cors = require('cors');

const MSSQL = require('./database/config');

// Crear el servidor de express
const app = express();

// Configurar CORS
app.use(cors());

// Lectura y parseo del body
app.use(express.json());

// Base de datos
MSSQL.instance().conexion
    .then(pool => console.log('==== Conexion exitosa a la base de datos ===='))
    .catch(error => console.log('Error al conectar a la base de datos', error));

// Directorio público
app.use(express.static('public'));

// Rutas
app.use('/api/usuarios', require('./routes/usuarios.routes'));
app.use('/api/contenedores', require('./routes/contenedores.routes'));
app.use('/api/bloques', require('./routes/bloques.routes'));
app.use('/api/bahias', require('./routes/bahias.routes'));
app.use('/api/movimientos', require('./routes/movimientos.routes'));
app.use('/api/login', require('./routes/auth.routes'));


app.listen(process.env.PORT, () => {
    console.log('Servidor corriendo en puerto ' + process.env.PORT);
});