/*
    Path: /api/contenedores
*/

const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

const { crearMovimiento, obtenerRelocalizaciones } = require('../controllers/movimiento.controller');

const router = Router();

router.post('/', validarJWT, crearMovimiento);

router.post('/relocalizacion/', validarJWT, obtenerRelocalizaciones)

module.exports = router;