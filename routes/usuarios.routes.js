/*
    Ruta: /api/usuarios
*/
const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

const { listarUsuarios, crearUsuario, actualizarUsuario, eliminarUsuario, obtenerUsuario } = require('../controllers/usuario.controller');

const router = Router();

router.get('/id', validarJWT, obtenerUsuario);

router.get('/', validarJWT, listarUsuarios);

router.post('/', validarJWT, [
        //check('nombre', 'El nombre es obligatorio').not().isEmpty(),
        //check('password', 'El password es obligatorio').not().isEmpty(),
        //check('email', 'El email es obligatorio').isEmail(),
        //validarCampos,
    ],
    crearUsuario
);

router.put('/:id', validarJWT, [
        //validarJWT,
        //check('nombre', 'El nombre es obligatorio').not().isEmpty(),
        //check('email', 'El email es obligatorio').isEmail(),
        //check('role', 'El role es obligatorio').not().isEmpty(),
        //validarCampos,
    ],
    actualizarUsuario
);

router.delete('/:id',
    validarJWT,
    eliminarUsuario
);

module.exports = router;