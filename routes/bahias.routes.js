/*
    Path: /api/bahias
*/

const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

const { obtenerBahiaPorBloqueContenedores, buscarBahias, obtenerBahia } = require('../controllers/bahia.controller');

const router = Router();

router.get('/bloque', validarJWT, obtenerBahiaPorBloqueContenedores);

router.get('/', validarJWT, buscarBahias);

router.get('/id', validarJWT, obtenerBahia);

module.exports = router;