/*
    Path: /api/login
*/
const { Router } = require('express');
const { iniciarSesion, renovarToken } = require('../controllers/auth.controller');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

const router = Router();

router.post('/', [
        //check('email', 'El email es obligatorio').isEmail(),
        //check('password', 'El password es obligatorio').not().isEmpty(),
        //validarCampos
    ],
    iniciarSesion
);
/*
router.post('/google', [
        check('token', 'El token de Google es obligatorio').not().isEmpty(),
        validarCampos
    ],
    googleSignIn
)*/

router.get('/renew',
    validarJWT,
    renovarToken
)

module.exports = router;