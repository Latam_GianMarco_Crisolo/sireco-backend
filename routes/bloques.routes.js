/*
    Path: /api/bloques
*/

const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

const { obtenerBloque, listarBloques, crearBloque, actualizarBloque, eliminarBloque } = require('../controllers/bloque.controller');

const router = Router();

router.get('/id', validarJWT, obtenerBloque);

router.get('/', validarJWT, listarBloques);

router.post('/', validarJWT, crearBloque);

router.put('/:id', validarJWT, actualizarBloque);

router.delete('/:id', validarJWT, eliminarBloque);

module.exports = router;