/*
    Path: /api/contenedores
*/

const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

const { listarContenedores, crearContenedor, actualizarContenedor, eliminarContenedor, obtenerContenedor, buscarContenedores } = require('../controllers/contenedor.controller');

const router = Router();

router.get('/id', validarJWT, obtenerContenedor);

router.get('/', validarJWT, listarContenedores);

router.get('/search', validarJWT, buscarContenedores);

router.post('/', validarJWT, crearContenedor);

router.put('/:id', validarJWT, actualizarContenedor);

router.delete('/:id', validarJWT, eliminarContenedor);

module.exports = router;