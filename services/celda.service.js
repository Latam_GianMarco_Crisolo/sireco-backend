const celdaModel = require('../models/celda.model');

const crearCelda = async(celdaDB) => {

    const celda = await celdaModel.buscarCeldaPorFilaColumnaBahia(celdaDB);

    if (celda) throw { ok: false, status: 400, msg: `Ya existe un celda con fila ${celdaDB.fila} y columna ${celdaDB.columna} para la bahía ${celdaDB.id_bahia}` };

    const id = await celdaModel.crearCelda(celdaDB);
    celdaDB.id = id;

    return celdaDB;
}

const obtenerCeldasPorBahia = async(idBahia) => {
    const celdasDB = await celdaModel.buscarCeldasPorBahia(idBahia);

    return celdasDB;
}

const obtenerCeldaPorFilaColumnaBahia = async(celdaDB) => {
    const celda = await celdaModel.buscarCeldaPorFilaColumnaBahia(celdaDB);

    return celda;
}

const actualizarContenedor = async(celdaDB) => {
    const id = await celdaModel.actualizarContenedor(celdaDB);

    return id;
}

const actualizarContenedorPorFilaColumnaBahia = async(celdaDB) => {
    const id = await celdaModel.actualizarContenedorPorFilaColumnaBahia(celdaDB);
    return id;
}


const eliminarCeldasPorBahia = async(idBahia) => {
    // Agregar Validación de si tiene contenedor asignado
    const idCeldasBorradas = await celdaModel.eliminarCeldasPorBahia(idBahia);
}

module.exports = {
    crearCelda,
    obtenerCeldasPorBahia,
    obtenerCeldaPorFilaColumnaBahia,
    actualizarContenedor,
    actualizarContenedorPorFilaColumnaBahia
}