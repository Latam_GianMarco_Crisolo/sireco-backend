const usuarioModel = require('../models/usuario.model');

const bcrypt = require('bcryptjs');

const iniciarSesion = async(usuario, contrasena) => {

    const usuarioDB = await usuarioModel.buscarUsuarioPorNombreUsuario(usuario);

    console.log("usuarioDB: ", usuarioDB);

    if (!usuarioDB) throw { ok: false, status: 400, msg: `No existe el usuario ${usuario}` };

    // Verificar contraseña
    const contrasenaValida = bcrypt.compareSync(contrasena, usuarioDB.contrasena);

    if (!contrasenaValida) throw { ok: false, status: 400, msg: `Contraseña no válida` };

    return usuarioDB;
}

const obtenerUsuario = async(id) => {
    const usuarioDB = await usuarioModel.buscarUsuarioPorId(id);

    if (!usuarioDB) throw { ok: false, status: 400, msg: `No existe usuario  con el id ${id}` };

    return usuarioDB;
}

const listarUsuarios = async(desde) => {
    const total = await usuarioModel.obtenerNumeroTotalUsuarios();

    const usuarios = await usuarioModel.listarUsuarios(desde);

    return { total, usuarios };
}

const crearUsuario = async(usuarioDB) => {
    const usuario = await usuarioModel.buscarUsuarioPorNombreUsuario(usuarioDB.usuario);

    if (usuario) throw { ok: false, status: 400, msg: `Ya existe el nombre de usuario ${usuarioDB.usuario}` };

    // Encriptar contraseña
    const salt = bcrypt.genSaltSync();
    usuarioDB.contrasena = bcrypt.hashSync(usuarioDB.contrasena, salt);

    const id = await usuarioModel.crearUsuario(usuarioDB);
    usuarioDB.id = id;

    return usuarioDB;
}

const actualizarUsuario = async(usuarioDB) => {

    let usuario = await usuarioModel.buscarUsuarioPorId(usuarioDB.id);

    if (!usuario) throw { ok: false, status: 400, msg: `No existe usuario con el id ${usuarioDB.id}` };

    //usuario = await usuarioModel.buscarUsuarioPorNombreUsuario(usuarioDB.usuario);
    //if (!usuario || usuario.id != usuarioDB.id) throw { ok: false, status: 400, msg: `Ya existe el nombre de usuario  ${usuarioDB.usuario}` };

    const id = await usuarioModel.actualizarUsuario(usuarioDB);

    return usuarioDB;
}

const eliminarUsuario = async() => {
    const idUsuarioBorrado = await usuarioModel.eliminarUsuario(id);

    if (!idContenedorBorrado) throw { ok: false, status: 400, msg: `No existe usuario con el id ${id}` };
}

module.exports = {
    iniciarSesion,
    obtenerUsuario,
    listarUsuarios,
    crearUsuario,
    actualizarUsuario,
    eliminarUsuario
}