const movimientoDetalleModel = require('../models/movimientoDetalle.model');

const crearMovimientoDetalle = async(movimientoDetalleDB) => {

    const id = await movimientoDetalleModel.crearMovimientoDetalle(movimientoDetalleDB);
    return id;
}

module.exports = {
    crearMovimientoDetalle
}