const bloqueModel = require('../models/bloque.model');

const obtenerBloque = async(idBloque) => {
    const bloque = await bloqueModel.buscarBLoquePorIdConTotalBahias(idBloque);

    if (!bloque) throw { ok: false, status: 400, msg: `No existe bloque de contenedor con el id ${idBloque}` };

    return bloque;
}

const buscarBloquePorId = async(idBloque) => {
    const bloque = await bloqueModel.buscarBloquePorId(idBloque);

    if (!bloque) throw { ok: false, status: 400, msg: `No existe bloque de contenedor con el id ${idBloque}` };

    return bloque;
}

const listarBloques = async(desde) => {
    const total = await bloqueModel.obtenerNumeroTotalBloques();

    const bloques = await bloqueModel.listarBloquesConTotalBahias(desde);

    return { total, bloques };
}

const crearBloque = async(bloqueDB) => {

    const bloque = await bloqueModel.buscarBloquePorCodigo(bloqueDB.codigo);

    if (bloque) throw { ok: false, status: 400, msg: `Ya existe un bloque con código ${bloqueDB.codigo}` };

    const id = await bloqueModel.crearBloque(bloqueDB);
    bloqueDB.id = id;

    return bloqueDB;
}

const eliminarBloque = async(id) => {
    const idBloqueBorrado = await bloqueModel.eliminarBloque(id);

    if (!idBloqueBorrado) throw { ok: false, status: 400, msg: `No existe bloque con el id ${id}` };
}

module.exports = {
    obtenerBloque,
    buscarBloquePorId,
    listarBloques,
    crearBloque,
    eliminarBloque
}