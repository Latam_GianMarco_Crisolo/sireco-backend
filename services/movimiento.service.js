const movimientoModel = require('../models/movimiento.model');

const crearMovimiento = async(movimientoDB) => {

    const id = await movimientoModel.crearMovimiento(movimientoDB);
    return id;
}

module.exports = {
    crearMovimiento
}