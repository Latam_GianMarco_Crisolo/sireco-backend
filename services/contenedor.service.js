const contenedorModel = require('../models/contenedor.model');

const obtenerContenedor = async(idContenedor) => {

    const contenedor = await contenedorModel.buscarContenedorPorId(idContenedor);

    if (!contenedor) throw { ok: false, status: 400, msg: `No existe contenedor con el id ${idContenedor}` };

    return contenedor;
}

const listarContenedores = async(desde) => {

    const total = await contenedorModel.obtenerNumeroTotalContenedores();

    const contenedores = await contenedorModel.listarContenedores(desde);

    return { total, contenedores };
}

const buscarContenedores = async(codigo, estado, embarcacion) => {
    const contenedores = await contenedorModel.buscarContenedores(codigo, estado, embarcacion);

    return contenedores;
}

const crearContenedor = async(contenedorDB) => {

    const contenedor = await contenedorModel.buscarContenedorPorCodigo(contenedorDB.codigo);

    if (contenedor) throw { ok: false, status: 400, msg: `Ya existe un contenedor con código ${contenedorDB.codigo}` };


    const id = await contenedorModel.crearContenedor(contenedorDB);
    contenedorDB.id = id;

    return contenedorDB;
}

const actualizarContenedor = async(contenedorDB) => {

    let contenedor = await contenedorModel.buscarContenedorPorId(contenedorDB.id);

    if (!contenedor) throw { ok: false, status: 400, msg: `No existe contenedor con el id ${contenedorDB.id}` };


    contenedor = await contenedorModel.buscarContenedorPorCodigo(contenedorDB.codigo);

    if (!contenedor || contenedor.id != contenedorDB.id) throw { ok: false, status: 400, msg: `Ya existe un contenedor con el codigo ${contenedorDB.codigo}` };


    const id = await contenedorModel.actualizarContenedor(contenedorDB);

    return contenedorDB;
}

const actualizarEstado = async(idContenedor, estado) => {
    const id = await contenedorModel.actualizarEstado(idContenedor, estado);

    return id;
}

const eliminarContenedor = async(id) => {
    const idContenedorBorrado = await contenedorModel.eliminarContenedor(id);

    if (!idContenedorBorrado) throw { ok: false, status: 400, msg: `No existe contenedor con el id ${id}` };
}

module.exports = {
    obtenerContenedor,
    listarContenedores,
    buscarContenedores,
    crearContenedor,
    actualizarContenedor,
    actualizarEstado,
    eliminarContenedor
}