const bahiaModel = require('../models/bahia.model');

const crearBahia = async(bahiaDB) => {

    const bahia = await bahiaModel.buscarBahiaPorCodigo(bahiaDB.codigo);

    if (bahia) throw { ok: false, status: 400, msg: `Ya existe un bloque con código ${bahiaDB.codigo}` };

    const id = await bahiaModel.crearBahia(bahiaDB);
    bahiaDB.id = id;

    return bahiaDB;
}

const obtenerBahia = async(idBahia) => {

    const bahia = await bahiaModel.buscarBahiaPorId(idBahia);

    if (!bahia) throw { ok: false, status: 400, msg: `No existe bahia con el id ${idBahia}` };

    return bahia;
}

const buscarBahiasPorBloque = async(idBloque) => {
    const bahias = await bahiaModel.buscarBahiasPorBloque(idBloque);
    return bahias;
}

const buscarBahias = async(bloque, bahia, contenedor) => {
    const bahias = await bahiaModel.buscarBahias(bloque, bahia, contenedor);
    return bahias;
}

const eliminarBahiasPorBloque = async(idBloque) => {
    const idBahiasBorradas = await bahiaModel.eliminarBahiasPorBloque(idBloque);
}

module.exports = {
    crearBahia,
    buscarBahias,
    buscarBahiasPorBloque,
    obtenerBahia,
    eliminarBahiasPorBloque
}