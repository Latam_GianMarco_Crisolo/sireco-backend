const bahiaService = require('../services/bahia.service');
const celdaService = require('../services/celda.service');
const bloqueService = require('../services/bloque.service');

const obtenerBahiaPorBloqueContenedores = async(req, res) => {
    try {
        console.log("BahiaController: obtenerBahiaPorBloqueContenedores");
        const { id } = req.query;

        const bahias = await bahiaService.buscarBahiasPorBloque(id);

        return res.status(200).json({
            ok: true,
            bahias
        });
    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

const obtenerBahia = async(req, res) => {
    try {
        console.log("BahiaController: obtenerBahia");
        const { id } = req.query;

        const bahia = await bahiaService.obtenerBahia(id);

        console.log(bahia);
        const bloque = await bloqueService.buscarBloquePorId(bahia.id_bloque_contenedores);

        console.log(bloque);
        const celdas = await celdaService.obtenerCeldasPorBahia(bahia.id);

        console.log("celdas", celdas);
        bahia.bloque = bloque;
        bahia.celdas = celdas;
        bahia.nroContenedores = celdas.length;

        return res.status(200).json({
            ok: true,
            bahia
        });

    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

const buscarBahias = async(req, res) => {
    try {
        console.log("BahiaController: buscarBahias");
        const { bloque, bahia, contenedor } = req.query;

        console.log(bloque, bahia, contenedor);

        const bahias = await bahiaService.buscarBahias(bloque, bahia, contenedor);

        return res.status(200).json({
            ok: true,
            bahias
        });

    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

module.exports = {
    obtenerBahiaPorBloqueContenedores,
    buscarBahias,
    obtenerBahia
}