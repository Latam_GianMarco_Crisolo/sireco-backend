const { response } = require('express');

const contenedorService = require('../services/contenedor.service');
const celdaService = require('../services/celda.service');
const movimientoService = require('../services/movimiento.service');
const movimientoDetalleService = require('../services/movimientoDetalle.service');


const crearMovimiento = async(req, res = response) => {
    try {

        console.log("MovimientoController: crearMovimiento");

        console.log('req', req.body);

        if (req.body.celdas === undefined) {
            // Retiro de Contenedores
            console.log("ENTRO A IF");


            let movimientoDB = req.body;

            console.log("movimientos: ", req.body);

            const idMovimiento = await movimientoService.crearMovimiento(movimientoDB);
            console.log("idMovimiento", idMovimiento);

            let movimientoDetalleDB = {},
                celdaDB;
            for (var i = 0; i < movimientoDB.detalle.length; i++) {
                movimientoDetalleDB.id_celda_inicio = movimientoDB.detalle[i].idCeldaInicio;
                movimientoDetalleDB.id_celda_fin = movimientoDB.detalle[i].idCeldaFin;
                movimientoDetalleDB.id_contenedor = movimientoDB.detalle[i].idContenedor;
                movimientoDetalleDB.tipo = movimientoDB.detalle[i].tipo;
                movimientoDetalleDB.id_movimiento = idMovimiento;
                console.log("movimientoDetalleDB : " + i, movimientoDetalleDB);
                movimientoDetalleDB.id = await movimientoDetalleService.crearMovimientoDetalle(movimientoDetalleDB);

                if (movimientoDetalleDB.id_celda_inicio)  {
                    celdaDB = {};
                    celdaDB.id = movimientoDetalleDB.id_celda_inicio;
                    celdaDB.id_contenedor = null;
                    console.log("celdaDB : " + i, celdaDB);

                    celdaDB.id = await celdaService.actualizarContenedor(celdaDB);
                }

                if (movimientoDetalleDB.id_celda_fin) {
                    celdaDB = {};
                    celdaDB.id = movimientoDetalleDB.id_celda_fin;
                    celdaDB.id_contenedor = movimientoDetalleDB.id_contenedor;
                    celdaDB.fila = movimientoDB.detalle[i].celdaFin.fila;
                    celdaDB.columna = movimientoDB.detalle[i].celdaFin.columna;
                    celdaDB.id_bahia = movimientoDB.detalle[i].celdaFin.idBahia;

                    celdaDB.id = await celdaService.actualizarContenedorPorFilaColumnaBahia(celdaDB);
                }

                if (!movimientoDetalleDB.id_celda_fin && movimientoDetalleDB.tipo == 'S') {
                    contenedorService.actualizarEstado(movimientoDetalleDB.id_contenedor, 'A');
                }
            }

            return res.status(200).json({
                ok: true,
                movimiento: movimientoDB
            });

        } else {
            // Asignación de Contenedores

            let celdasDB = req.body.celdas;
            // A: Normal, R: Relocalización
            let movimientoDB = {
                tipo: req.body.tipo
            };
            console.log("ENTRO A ELSE");


            // Verificar que realmente hubo cambio dentro de la bahía.

            const idMovimiento = await movimientoService.crearMovimiento(movimientoDB);
            console.log('idMovimiento: ', idMovimiento);
            let movimientoDetalleDB, celdaFinDB, hayMovimiento;

            for (var i = 0; i < celdasDB.length; i++) {
                movimientoDetalleDB = {};
                console.log('===========================' + i);
                celdasDB[i].id_bahia = celdasDB[i].idBahia;

                hayMovimiento = true;
                movimientoDetalleDB.id_movimiento = idMovimiento;
                console.log('1');
                if (!celdasDB[i].id) {
                    //E: Entrada, I: Interno, S: Salida.
                    movimientoDetalleDB.tipo = 'E';
                    celdaFinDB = await celdaService.obtenerCeldaPorFilaColumnaBahia(celdasDB[i]);
                    console.log('celda obtenida: ', celdaFinDB);
                    movimientoDetalleDB.id_celda_inicio = null;
                    movimientoDetalleDB.id_celda_fin = celdaFinDB.id;
                    celdaFinDB.id_contenedor = celdasDB[i].idContenedor;
                    console.log("actualizarContenedor: TIPO E" + i, celdaFinDB);
                    await celdaService.actualizarContenedor(celdaFinDB);
                } else {
                    celdaFinDB = await celdaService.obtenerCeldaPorFilaColumnaBahia(celdasDB[i]);
                    if (celdasDB[i].id != celdaFinDB.id) {
                        celdaFinDB.id_contenedor = celdasDB[i].contenedor.id_contenedor;
                        celdasDB[i].id_contenedor = null;
                        // Quitar Contenedor de Celda de Inicio
                        console.log("actualizarContenedor Inicio: TIPO I" + i, celdasDB[i]);
                        //await celdaService.actualizarContenedor(celdasDB[i]);
                        // Poner Contenedor a Celda Fin
                        console.log("actualizarContenedor Fin: TIPO I" + i, celdaFinDB);
                        //await celdaService.actualizarContenedor(celdaFinDB);

                        movimientoDetalleDB.tipo = 'I';
                        movimientoDetalleDB.id_celda_inicio = celdasDB[i].id;
                        movimientoDetalleDB.id_celda_fin = celdaFinDB.id;
                    } else {
                        hayMovimiento = false;
                    }
                }
                movimientoDetalleDB.id_contenedor = celdasDB[i].idContenedor;
                if (hayMovimiento) {
                    console.log("crearMovimientoDetalle: " + i, movimientoDetalleDB);
                    if (movimientoDetalleDB.tipo == 'E')  {
                        // A: Asignado, N: No Asignado
                        contenedorService.actualizarEstado(movimientoDetalleDB.id_contenedor, 'A');
                    }
                    await movimientoDetalleService.crearMovimientoDetalle(movimientoDetalleDB);
                }
            }
            return res.status(200).json({
                ok: true
            });
        }



    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

const obtenerRelocalizaciones = async(req, res = response) => {

    try {

        console.log("MovimientoController: obtenerRelocalizaciones");

        //console.log('req', req.body);

        const celdas = req.body;

        console.log("==================");
        //obtenerPrioridad(celdas);

        // obtenerMovimientos(celdas);


        let detalle = [];
        movimientoDetalle = {};

        movimientoDetalle.tipo = 'I';
        movimientoDetalle.idContenedor = 13;
        movimientoDetalle.idCeldaInicio = 292;
        movimientoDetalle.idCeldaFin = 297;
        movimientoDetalle.contenedor = buscarContenedor(movimientoDetalle.idContenedor, celdas);
        movimientoDetalle.celdaInicio = buscarCelda(movimientoDetalle.idCeldaInicio, celdas);
        movimientoDetalle.celdaFin = buscarCelda(movimientoDetalle.idCeldaFin, celdas);
        detalle.push(movimientoDetalle);

        movimientoDetalle = {};
        movimientoDetalle.tipo = 'I';
        movimientoDetalle.idContenedor = 10;
        movimientoDetalle.idCeldaInicio = 296;
        movimientoDetalle.idCeldaFin = 292;
        movimientoDetalle.contenedor = buscarContenedor(movimientoDetalle.idContenedor, celdas);
        movimientoDetalle.celdaInicio = buscarCelda(movimientoDetalle.idCeldaInicio, celdas);
        movimientoDetalle.celdaFin = buscarCelda(movimientoDetalle.idCeldaFin, celdas);
        detalle.push(movimientoDetalle);

        movimientoDetalle = {};
        movimientoDetalle.tipo = 'I';
        movimientoDetalle.idContenedor = 8;
        movimientoDetalle.idCeldaInicio = 293;
        movimientoDetalle.idCeldaFin = 295;
        movimientoDetalle.contenedor = buscarContenedor(movimientoDetalle.idContenedor, celdas);
        movimientoDetalle.celdaInicio = buscarCelda(movimientoDetalle.idCeldaInicio, celdas);
        movimientoDetalle.celdaFin = buscarCelda(movimientoDetalle.idCeldaFin, celdas);
        detalle.push(movimientoDetalle);


        //console.log(detalle);

        return res.status(200).json({
            ok: true,
            detalle
        });

    } catch (error) {

    }
}

const buscarContenedor = (idContenedor, celdas) => {
    for (let i = 0; i < celdas.length; i++) {
        if (celdas[i].idContenedor == idContenedor) {
            return celdas[i].contenedor;
        }
    }
}

const buscarCelda = (idCelda, celdas) => {
    for (let i = 0; i < celdas.length; i++) {
        if (celdas[i].id == idCelda) {
            return {
                fila: celdas[i].fila,
                columna: celdas[i].columna,
                id: celdas[i].id,
                idBahia: celdas[i].idBahia,
            }
        }
    }
}


const obtenerPrioridad = (celdas) => {

    let auxiliar;
    for (let i = 0; i < celdas.length - 1; i++) {
        for (let j = i + 1; j < celdas.length; j++) {
            if (new Date(celdas[i].contenedor.fechaSalida) > new Date(celdas[j].contenedor.fechaSalida)) {
                auxiliar = celdas[i].contenedor.fechaSalida;
                celdas[i].contenedor.fechaSalida = celdas[j].contenedor.fechaSalida;
                celdas[j].contenedor.fechaSalida = auxiliar;
            }
        }
    }


    let pivote = celdas[0].contenedor.fechaSalida;
    let prioridad = 1;
    for (let i = 0; i < celdas.length; i++) {
        if (pivote != celdas[i].contenedor.fechaSalida) {
            prioridad++;
            pivote = celdas[i].contenedor.fechaSalida;
        }
        celdas[i].prioridad = prioridad;
    }
}

const obtenerMovimientos = (celdas) => {

    const bahia = obtenerBahia(celdas);

    const { colMalas, colBuenas } = obtenerColumnas(bahia);
}

const obtenerBahia = (celdas) => {

    try {
        let nuevaBahia = [];
        for (let i = 0; i < celdas[0].bahia.bloque.columnas; i++) {
            nuevaBahia[i] = [];
        }

        console.log("NuevaBahia: ", nuevaBahia);

        for (let i = 0; i < celdas.length; i++) {
            nuevaBahia[celdas[i].columna - 1][celdas[i].fila - 1] = celdas[i];
        }
        return nuevaBahia;
    } catch (error) {
        console.log(error);
    }
}

const obtenerColumnas = (bahia) => {
    let prioridad = 0;
}

module.exports = {
    crearMovimiento,
    obtenerRelocalizaciones
}