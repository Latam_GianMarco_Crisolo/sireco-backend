const { response } = require('express');

const { generarJWT } = require('../helpers/jwt');

const MSSQL = require('../database/config');

const usuarioService = require('../services/usuario.service');

const iniciarSesion = async(req, res = response) => {

    try {
        console.log('POST: iniciarSesion')
        const { usuario, contrasena } = req.body;

        const usuarioDB = await usuarioService.iniciarSesion(usuario, contrasena);
        delete usuarioDB.contrasena;

        // Generar el TOKEN - JWT
        const token = await generarJWT(usuarioDB.id);

        res.status(200).json({
            ok: true,
            msg: 'Login correcto',
            usuario: usuarioDB,
            id: usuarioDB.id,
            token: token
        });

    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

const renovarToken = async(req, res = response) => {

    try {
        const id = req.uid;

        // Generar el TOKEN - JWT
        const token = await generarJWT(id);

        const usuarioDB = await usuarioService.obtenerUsuario(id);

        res.json({
            ok: true,
            usuario: usuarioDB,
            token,
        });

    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

module.exports = {
    iniciarSesion,
    renovarToken
}