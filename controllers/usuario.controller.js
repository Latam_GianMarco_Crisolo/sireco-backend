const { response } = require('express');

const usuarioService = require('../services/usuario.service');

const obtenerUsuario = async(req, res) => {

    try {
        console.log("UsuarioController: obtenerUsuario");
        const { id } = req.query;

        const usuario = await usuarioService.obtenerUsuario(id);

        return res.status(200).json({
            ok: true,
            usuario
        });

    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

const listarUsuarios = async(req, res) => {

    try {
        console.log("UsuarioController: listarUsuarios");

        const { desde } = req.query;

        const { total, usuarios } = await usuarioService.listarUsuarios(desde);

        return res.status(200).json({
            ok: true,
            usuarios,
            total
        });

    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

const crearUsuario = async(req, res = response) => {

    try  {
        console.log("UsuarioController: crearUsuario");

        let usuarioDB = req.body;
        usuarioDB['apellido_pat'] = usuarioDB.apellidoPaterno;
        usuarioDB['apellido_mat'] = usuarioDB.apellidoMaterno;
        delete usuarioDB.apellidoPaterno;
        delete usuarioDB.apellidoMaterno;

        const usuario = await usuarioService.crearUsuario(usuarioDB);

        delete usuario.contrasena

        return res.status(200).json({
            ok: true,
            usuario
        });
    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

const actualizarUsuario = async(req, res = response) => {


    try {
        console.log("UsuarioController: actualizarUsuario");

        const usuarioDB = req.body;
        delete usuarioDB.contrasena;

        let usuario = await usuarioService.actualizarUsuario(usuarioDB);

        return res.status(200).json({
            ok: true,
            usuario
        });

    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

const eliminarUsuario = async(req, res = response) => {

    try {
        console.log("UsuarioController: eliminarUsuario");

        const { id } = req.params;

        await usuarioService.eliminarUsuario(id);

        return res.status(200).json({
            ok: true,
            msg: 'usuario eliminado'
        });
    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

module.exports = {
    listarUsuarios,
    crearUsuario,
    actualizarUsuario,
    eliminarUsuario,
    obtenerUsuario
}