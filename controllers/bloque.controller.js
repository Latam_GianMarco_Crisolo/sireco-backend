const { response } = require('express');

const MSSQL = require('../database/config');

const bloqueService = require('../services/bloque.service');
const bahiaService = require('../services/bahia.service');
const celdaService = require('../services/celda.service');

const obtenerBloque = async(req, res) => {
    try {
        console.log("BloqueController: obtenerBloque");
        const { id } = req.query;

        const bloque = await bloqueService.obtenerBloque(id);

        return res.status(200).json({
            ok: true,
            bloque
        });

    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

const listarBloques = async(req, res) => {

    try {
        console.log("BloqueController: listarBloques");

        const { desde } = req.query;

        console.log(req.query);
        const { total, bloques } = await bloqueService.listarBloques(desde);

        return res.status(200).json({
            ok: true,
            bloques,
            total
        });

    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

const crearBloque = async(req, res = response) => {

    try  {
        console.log("BloqueController: crearBloque");

        let bloqueDB = req.body;

        const bloque = await bloqueService.crearBloque(bloqueDB);

        let bahiaDB;
        for (let i = 1; i <= bloqueDB.numBahias; i++) {
            bahiaDB = {
                codigo: bloque.codigo + '-' + completar_digitos(2, i),
                numero: i,
                estado: 'A',
                id_bloque_contenedores: bloque.id
            }
            console.log('bahia: ' + i, bahiaDB);

            const bahia = await bahiaService.crearBahia(bahiaDB);

            let celdaDB;
            for (let fila = 1; fila <= bloqueDB.filas; fila++) {
                for (let columna = 1; columna <= bloqueDB.columnas; columna++) {
                    celdaDB = {
                        fila,
                        columna,
                        id_bahia: bahiaDB.id
                    }
                    celdaDB = await celdaService.crearCelda(celdaDB);
                }
            }
        }

        return res.status(200).json({
            ok: true,
            bloque
        });
    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

const actualizarBloque = async(req, res = response) => {
    let terminarProceso = false;

    const { id } = req.params;
    const { codigo } = req.body;

    let bloqueDBAnterior;

    console.log('params', { id, codigo })
    await MSSQL.instance().conexion.then(pool => pool.query(`select bc.*, count(b.id) as numBahias from bloque_contenedores bc left join bahia b on bc.id = b.id_bloque_contenedores where bc.id = '${id}' or (bc.codigo = '${codigo}' and bc.id != '${id}') GROUP BY bc.id, bc.codigo, bc.columnas, bc.estado, bc.filas`))
        .then(result => {
            console.log('result.recordset[0].codigo', result.recordset);
            if (!result.recordset.length) {
                terminarProceso = true;
                return res.status(400).json({
                    ok: false,
                    msg: 'No existe un bloque con ese id'
                });
            } else {
                for (var i = 0; i < result.recordset.length; i++) {
                    if (result.recordset[i].codigo == codigo && result.recordset[i].id != id) {
                        terminarProceso = true;
                        return res.status(400).json({
                            ok: false,
                            msg: 'El codigo del bloque ingresado ya existe'
                        });
                    } else {
                        bloqueDBAnterior = result.recordset[i];
                    }
                }
            }
        }).catch(error => {
            terminarProceso = true;
            console.log(error);
            return res.status(500).json({
                ok: false,
                msg: 'Error inesperado... revisar logs'
            });
        });

    console.log('terminarProceso', terminarProceso, bloqueDBAnterior);

    if (terminarProceso) return;

    let bloqueDB = req.body;

    let query = 'UPDATE bloque_contenedores SET' +
        ` codigo = '${bloqueDB.codigo}',` +
        ` filas = ${bloqueDB.filas},` +
        ` columnas = ${bloqueDB.columnas},` +
        ` estado = '${bloqueDB.estado}'` +
        ` WHERE id = ${id}`;

    await MSSQL.instance().conexion.then(pool => pool.query(query))
        .then(result => {
            if (!result.rowsAffected[0]) {
                terminarProceso = true;
                res.status(404).json({
                    ok: true,
                    msg: 'No existe un bloque de contenedor con este id'
                });
                return;
            }
        }).catch(error => {
            terminarProceso = true;
            console.log(error);
            return res.status(500).json({
                ok: false,
                msg: 'Error inesperado... revisar logs'
            });
        });


    if (terminarProceso) return;

    if (bloqueDB.numBahias > bloqueDBAnterior.numBahias) {
        for (let i = bloqueDBAnterior.numBahias + 1; i <= bloqueDB.numBahias; i++) {
            console.log('entro for');

            var bahiaDB = {
                codigo: bloqueDB.codigo + completar_digitos(2, i),
                numero: i,
                estado: 'V',
                id_bloque_contenedor: id
            }
            console.log('bahia: ' + i, bahiaDB);

            query = `INSERT INTO bahia (codigo, numero, id_bloque_contenedores, estado) OUTPUT INSERTED.id VALUES ('${bahiaDB.codigo}', ${bahiaDB.numero}, ${bahiaDB.id_bloque_contenedor}, '${bahiaDB.estado}')`;

            await MSSQL.instance().conexion.then(pool => pool.query(query))
                .then(result => {
                    if (!result.rowsAffected[0]) {
                        terminarProceso = true;
                        return res.status(500).json({
                            ok: false,
                            msg: 'Error inesperado... revisar logs'
                        });
                    }
                    bloqueDB.id = result.recordset[0].id;
                })
                .catch(error => {
                    console.log(error);
                    return res.status(500).json({
                        ok: false,
                        msg: 'Error inesperado... revisar logs'
                    });
                });
            /*
            for (let f = 1; f < bloqueDB.filas; f++) {
                for (let c = 1; c < bloqueDB.columnas; c++) {
                    query = `INSERT INTO celda (fila, columna, id_bahia ) VALUES (${f}, ${c}, ${bahiaDB.id})`;


                }
            }*/
        }
    } else if (bloqueDB.numBahias < bloqueDBAnterior.numBahias) {

        query = `DELETE FROM bahia WHERE numero > ${bloqueDB.numBahias}`;

        await MSSQL.instance().conexion.then(pool => pool.query(query))
            .then(result => {
                if (!result.rowsAffected[0]) {
                    return res.status(500).json({
                        ok: false,
                        msg: 'Error inesperado... revisar logs'
                    });
                }
            }).catch(error => {
                console.log(error);
                return res.status(500).json({
                    ok: false,
                    msg: 'Error inesperado... revisar logs'
                });
            });
    }

    /*
        anterior columnas 1 filas 2
        celda 1 1, 1 2

        ahora columnas 2 filas 2
        por crear 2 1 y 2 2

        anterior columna 2 fila 1
        celda 1 1, 2 1

        ahora columna 2 fila 2
        por crear 1 2, 2 2

        anterior columna 2 fila 2
        celda 1 1, 1 2, 2 1, 2 2

        ahora columna 3 fila 3
        por crear 1 3, 2 3, 3 3
        por crear 3 1, 3 2, 3 3

    */

    /*
    if (bloqueDB.filas > bloqueDBAnterior.filas) {
        for (let c = 1; c <= bloqueDBAnterior.columnas; c++) {
            for (let f = bloqueDB.filas; f > bloqueDBAnterior.filas; f--) {
                query = `INSERT INTO celda (fila, columna, id_bahia ) VALUES (${f}, ${c}, ${bahiaDB.id})`;
                await MSSQL.instance().conexion.then(pool => pool.query(``))
                    .then(result => {
                        if (!result.recordset.length) {
                            return res.status(400).json({
                                ok: false,
                                msg: 'No existe un bloque de contenedor con ese id'
                            });
                        }
                        res.status(200).json({
                            ok: true,
                            bloque: result.recordset[0]
                        });

                    }).catch(error => {
                        console.log(error);
                        return res.status(500).json({
                            ok: false,
                            msg: 'Error inesperado... revisar logs'
                        });
                    });
            }
        }
    }
    if (bloqueDB.columnas > bloqueDBAnterior.columnas) {
        for (let f = 1; f <= bloqueDBAnterior.filas; f++) {
            for (let c = bloqueDB.columnas; c > bloqueDBAnterior.columnas; c--) {
                await MSSQL.instance().conexion.then(pool => pool.query(`select * from bloque_contenedores where id = '${id}'`))
                    .then(result => {
                        if (!result.recordset.length) {
                            return res.status(400).json({
                                ok: false,
                                msg: 'No existe un bloque de contenedor con ese id'
                            });
                        }
                        res.status(200).json({
                            ok: true,
                            bloque: result.recordset[0]
                        });

                    }).catch(error => {
                        console.log(error);
                        return res.status(500).json({
                            ok: false,
                            msg: 'Error inesperado... revisar logs'
                        });
                    });
            }
        }
    }

    if (bloqueDB.filas > bloqueDBAnterior.filas && bloqueDB.columnas > bloqueDBAnterior.columnas)  {
        for (let c = nc; c > columnas; c--) {
            for (let f = nf; f > filas; f--) {

            }
        }
    }

    */
    await MSSQL.instance().conexion.then(pool => pool.query(`select * from bloque_contenedores where id = '${id}'`))
        .then(result => {
            if (!result.recordset.length) {
                return res.status(400).json({
                    ok: false,
                    msg: 'No existe un bloque de contenedor con ese id'
                });
            }
            res.status(200).json({
                ok: true,
                bloque: result.recordset[0]
            });

        }).catch(error => {
            console.log(error);
            return res.status(500).json({
                ok: false,
                msg: 'Error inesperado... revisar logs'
            });
        });
}

const eliminarBloque = async(req, res = response) => {
    try {
        console.log("BloqueController: eliminarBloque");

        const { id } = req.params;

        bahias = await bahiaService.buscarBahiasPorBloque(id);
        if (bahias.length) {
            for (let i = 0; i < bahias.length; i++) {
                await celdaService.eliminarCeldasPorBahia(bahias[i].id);
            }
            await bahiaService.eliminarBahiasPorBloque(id);
        }
        await bloqueService.eliminarBloque(id);

        return res.status(200).json({
            ok: true,
            msg: 'contenedor eliminado'
        });
    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

const completar_digitos = (cant, numero) => {
    tamano = (numero + '').length;
    if (tamano < cant) {
        return '0' + numero;
    } else {
        return numero;
    }
}

module.exports = {
    obtenerBloque,
    listarBloques,
    crearBloque,
    actualizarBloque,
    eliminarBloque
}