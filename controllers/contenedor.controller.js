const { response } = require('express');

const contenedorService = require('../services/contenedor.service');

const obtenerContenedor = async(req, res) => {
    try {
        console.log("ContenedorController: obtenerContenedor");
        const { id } = req.query;

        const contenedor = await contenedorService.obtenerContenedor(id);

        return res.status(200).json({
            ok: true,
            contenedor
        });

    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

const listarContenedores = async(req, res) => {
    try {
        console.log("ContenedorController: listarContenedores");

        const { desde } = req.query;

        console.log(req.query);
        const { total, contenedores } = await contenedorService.listarContenedores(desde);

        return res.status(200).json({
            ok: true,
            contenedores,
            total
        });

    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

const buscarContenedores = async(req, res) => {
    try {

        console.log("ContenedorController: buscarContenedores");

        console.log('QUERY', req.query);
        const { codigo, embarcacion, estado } = req.query;

        const contenedores = await contenedorService.buscarContenedores(codigo, estado, embarcacion);

        return res.status(200).json({
            ok: true,
            contenedores
        });

    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

const crearContenedor = async(req, res = response) => {
    try  {
        console.log("ContenedorController: crearContenedores");

        let contenedorDB = req.body;
        contenedorDB.fecha_salida = contenedorDB.fechaSalida;
        delete contenedorDB.fechaSalida;

        const contenedor = await contenedorService.crearContenedor(contenedorDB);

        return res.status(200).json({
            ok: true,
            contenedor
        });
    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

const actualizarContenedor = async(req, res = response) => {
    try {
        console.log("ContenedorController: actualizarContenedor");

        let contenedorDB = req.body;
        contenedorDB.fecha_salida = contenedorDB.fechaSalida;
        delete contenedorDB.fechaSalida;

        let contenedor = await contenedorService.actualizarContenedor(contenedorDB);

        return res.status(200).json({
            ok: true,
            contenedor
        });

    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

const eliminarContenedor = async(req, res = response) => {
    try {
        console.log("ContenedorController: eliminarContenedor");

        const { id } = req.params;

        await contenedorService.eliminarContenedor(id);

        return res.status(200).json({
            ok: true,
            msg: 'contenedor eliminado'
        });
    } catch (error) {
        console.log('ERROR.... ,', error);
        return res.status(error.status).json(error);
    }
}

module.exports = {
    listarContenedores,
    crearContenedor,
    actualizarContenedor,
    eliminarContenedor,
    obtenerContenedor,
    buscarContenedores
}